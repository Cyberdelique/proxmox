#!/bin/bash
clear
echo "----------------------------------------------------------
-           installing cyberdelique's edition            -
----------------------------------------------------------"
sleep 2

sh 00-base.sh && sh 10-GUI.sh && sh 20-look.sh &&

# remove no subscription nag reminder

sed -i.bak "s/data.status !== 'Active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service

echo "install complete, get ready to enjoy this awsome release by Cyberdelique"

sleep 1

echo "reboot in 3"
sleep 1
echo "2..."
sleep 1
echo "1..."
sleep 1
echo "Shazam !!!"
sleep 1

reboot

## end of setup script ##
