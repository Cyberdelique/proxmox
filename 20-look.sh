#!/bin/bash

#copy i3 config
cd /root/proxmox/
mkdir /home/master/.config/
mkdir /home/master/.config/i3/
mkdir /home/master/.config/qutebrowser/
mkdir /home/master/.bin/
cp ./.config/i3/config /home/master/.config/i3/config
cp ./.bin/pxmx-webIface.sh /home/master/.bin
cp ./.config/qutebrowser/config.py /home/master/.config/qutebrowser
chown -R 1000:1000 /home/master/

#copy wallpaper
cd /root/proxmox/
mkdir /usr/share/backgrounds/
cp ./usr/share/backgrounds/server.jpg /usr/share/backgrounds/
cp ./boot/grub/themes/Shodan/wallpaper.png /usr/share/backgrounds/

#change grub theme
mkdir /boot/grub/themes
cd /root/proxmox/boot/grub/themes
cp -R ./Shodan /boot/grub/themes/
cp /etc/default/grub /etc/default/grub.bak 
sed "$ a GRUB_THEME=/boot/grub/themes/Shodan/Shodan/theme.txt" /etc/default/grub >/etc/default/grub1
mv /etc/default/grub1 /etc/default/grub
cp /boot/grub/themes/Shodan/Shodan/bgnd_1024x768.png /usr/share/desktop-base/active-theme/grub/grub-4x3.png
update-grub

#change lightdm theme
cd /etc/lightdm
cp /etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf.bak
sed -i 's|#background=|background=/boot/grub/themes/Shodan/wallpaper.png|' /etc/lightdm/lightdm-gtk-greeter.conf
sed -i 's|#greeter-hide-users=false|greeter-hide-users=false|' /etc/lightdm/lightdm.conf
sed -i 's|#autologin-user=|autologin-user=master|' /etc/lightdm/lightdm.conf

#define resolution
cd /root/proxmox
mkdir /home/master/.screenlayout
cp ./.screenlayout/1600.sh /home/master/.screenlayout/
cp ./.screenlayout/1920.sh /home/master/.screenlayout/
chown -R 1000:1000 /home/master/.screenlayout

#config nitrogen
cd /root/proxmox
cp -R ./.config/nitrogen /home/master/.config/nitrogen
chown -R 1000:1000 /home/master/.config/nitrogen

clear
echo "The GUI has been tweaked to cyberdelique's liking
use win+1-9 to change desktop
use win+return for a terminal
use win+shift+return fur a file manager
use win+shift+e to exit i3"

sleep 10
clear
## end of script 20 ##
