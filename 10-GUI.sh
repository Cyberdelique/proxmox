#!/bin/bash

# add lightdm and i3

apt install -y sudo lightdm i3 i3blocks suckless-tools stterm arandr nitrogen geany synaptic firefox-esr qutebrowser ranger pcmanfm virt-viewer spice-vdagent ifupdown2 curl xarchiver htop cmatrix

#add i3-gaps
cd /root
git clone https://github.com/maestrogerardo/i3-gaps-deb.git
cd ./i3-gaps-deb
chmod +x i3-gaps-deb
./i3-gaps-deb

#add user master
clear
echo "---------------------------------------------------------"
echo "-        please enter your new master password          -"
echo "---------------------------------------------------------"

adduser master
cd /usr/sbin
./usermod -aG sudo master

## display end of script 10 ##
clear
echo "Congrats ,you've now created the master user password and installed the i3 window manager and some other stuff"
sleep 5
clear
## end of script 10 ##
##
