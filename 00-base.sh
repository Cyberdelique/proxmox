#!/bin/bash

# comment out entreprise repo

cd /etc/apt/sources.list.d/
sed 's/^deb/#&/' pve-enterprise.list >pve-enterprise.list1
mv pve-enterprise.list1 pve-enterprise.list

# add no-subscription repository

cd /etc/apt/
sed -i '2i deb-src http://ftp.be.debian.org/debian buster main contrib' sources.list
sed -i '5i deb-src http://ftp.be.debian.org/debian buster-updates main contrib' sources.list
sed -i '7i deb http://download.proxmox.com/debian/pve buster pve-no-subscription' sources.list

# add bashtop repository

#cd /etc/apt/sources.list.d/
#echo "deb http://packages.azlux.fr/debian/ buster main" | tee /etc/apt/sources.list.d/azlux.list
#wget -qO - https://azlux.fr/repo.gpg.key | apt-key add -

# update distro

apt update && apt dist-upgrade -y

#display end of script 00
clear
echo "Phase 0 completed successfully
you've now switched to the free edition of proxmox and added the bashtop repository"
sleep 5
clear
## end of script 00 ##





